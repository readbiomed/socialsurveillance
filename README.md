# README #

This project will contain the published resources for the READ-BioMed Team "Social Surveillance" project, involving analysis of social media text for syndromic surveillance.

Current project contributors are Karin Verspoor and Bahadorreza Ofoghi from the University of Melbourne, Melbourne Australia.
Contact karin.verspoor@unimelb.edu.au for further information.

The Emotion vocabulary list is available in the repository in the resources folder:
>  ../resources/emotion_vocabulary.txt