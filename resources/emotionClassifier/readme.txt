*************Non-commercial use only*************

Developers:
This software has been developed by Bahadorreza Ofoghi, Meghan Mann, and Karin Verspoor at The University of Melbourne, Australia.

External required libraries (copy the following files in to \lib directory of this package):
ejml-0.23.jar
mallet.jar
mallet-deps.jar
stanford-corenlp-3.5.1.jar
stanford-corenlp-3.5.1-javadoc.jar
stanford-corenlp-3.5.1-models.jar
stanford-corenlp-3.5.1-sources.jar

About:
This software implements an emotion classifier for using Ekman's six basic emotions (anger, disgust, happiness, sadness, surprise, and fear), and extends it with three additional attitudinal
classes, sarcasm, news-related, and criticism.

Cite the software:
- Bahadorreza Ofoghi and Karin Verspoor. Textual emotion classification: An interoperability study on cross-genre data sets. In Proceedings of AI 2017, Advances in Artificial Intelligence: 30th Australasian Joint Conference, pp. 262-273, Melbourne, 2017.
- Bahadorreza Ofoghi, Meghan Mann, and Karin Verspoor. Towards early discovery of salient health threats: A social media emotion classification technique. In Proceedings of Pacific Symposium on Biocomputing (PSB 2016), Hawaii, 2016.

How to run:
To run the project from the command line, go to the dist folder and
type the following:

java -jar emotionClf.jar input_parameters (1 to 7)
input parameters (1 to 7) are:
//param 1: head directory where serialized classifiers are stored
//param 2: {0,1} 0=non-lemmatized, 1=lemmatized
//param 3: {0,1} 0=no-preprocess, 1=preprocess
//param 4: {0,...,6} 0=raw, 1=+emoticon, 2=+emotion vocabulary, 3=+punctuation, 4=+lexical rule-based classification features, 5=+sentiment, 6=+3+4+5
//param 5: input stop word file: one token per line
//param 6: input text file: one instance per line
//param 7: out text file: one classified instance per line: class + TAB + instance
